(defun org-insert-properties-drawer-with-custom-id-and-class (cid cls)
  "Create a CUSTOM_ID <cid> and CLASS <cls> for the current entry and return it"
  (interactive "sCUSTOM_ID: \nsCLASS: ")
  (org-insert-drawer 1 "PROPERTIES")
  (org-entry-put (point) "CUSTOM_ID" cid)
  (org-entry-put (point) "CLASS" cls))

(define-key org-mode-map "\C-xp" 'org-insert-properties-drawer-with-custom-id-and-class)
  
(defun org-custom-id-get-create (&optional force)
  "Create an CUSTOM_ID for the current entry and return it.
If the entry already has an CUSTOM-ID, just return it.
With optional argument FORCE, force the creation of a new CUSTOM_ID."
  (interactive "P")
  (when force
    (org-entry-put (point) "CUSTOM_ID" nil))
  (org-id-get (point) 'create))


(defun org-convert-csv-table (beg end)
  (interactive (list (mark) (point)))
  (org-table-convert-region beg end ",")
  )
