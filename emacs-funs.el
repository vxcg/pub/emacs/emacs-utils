;;; Emacs function utilities
;;; Author:  Venkatesh Choppella <vxc@gitlab>
;;; License:  GNU GPL v3

(defun as-dir (d)
  (file-name-as-directory d))

(defun concat-as-dir (d l)
  (as-dir (concat (as-dir d) l)))



;;; lp is a colon separated string of emacs directories
;;; "foo/elisp/:bar/:."
(defun prepend-path-string-to-load-path (lp)
  (setq load-path
		(append (split-string lp ":")
				load-path)))


;;; pretty-prints a list of strings, one line at a time.
(defun pprint-los (ls)
  (mapconcat 'identity ls  "\n"))

(defun pprint-load-path ()
  (pprint-los load-path))

(defun cmd (&rest args)
  (let ((c (apply 'format args)))
	(message c)
	(shell-command c)))


(defun load-path-str-to-list (lp)
  (split-string lp ":"))


;;; string utilities
;;; ================

(defun tar-gz-p (fname)
  (and (stringp fname)
	   (string-suffix-p ".tar.gz" fname)))

;;; returns the string c such that 
;;; a = cb, 
;;; nil otherwise
(defun string-chop-suffix (a b)
  (if (string-suffix-p b a)
	  (substring a 0 (- (length a) (length b)))))

;;; returns the string c such that
;;; a = bc
(defun string-chop-prefix (a b)
  (if (string-prefix-p b a)
	  (substring a (length b) (length a))))


;;; lookup key k in assoc-list d.  Return v if k isn't present.
(defun lookup (k d &optional v)
  (let ((p (assoc k d)))
	(if p (cdr p)
	  v)))

(defun online-p ()
  (= 0 (cmd
		"ping -c 1 www.google.com >&/dev/null; echo $?"
;;		"nc -zvw1 google.com 443"
		)))

(require 'url)

(defun tar-gz-repo-p (url)
  (and (stringp url)
	   (string-suffix-p ".tar.gz" url)))

(defun git-bare-repo-p (url)
  (and (stringp url)
	   (string-suffix-p ".git" url)))
						
(defun git-bare-repo-to-base (repo)
  (let ((full-filename-git
		 (url-filename (url-generic-parse-url repo))))
	(file-name-base full-filename-git)))
  
(defun git-install (base-dir repo &optional name)
  "Installs the git repository at REPO at the directory obtained
  by joining BASE-DIR with OPTIONAL NAME and returns that
  directory.  Returns nil if the operation fails."
  ;; directory of working copy
  (let* ((wsn (or name (git-bare-repo-to-base repo)))
		 (wsd (concat-as-dir base-dir wsn)))
	(cond ((file-exists-p wsd)
		   (message "%s already installed. " wsd)
		   (cond ((online-p)
				  (message "online.  pulling ...")
				  (cmd "git -C %s pull origin master" wsd) wsd)
				 (t
				  (message "offline.  Try pulling later.") wsd)))
		  ((online-p)
		   (message "online.  cloning ...")
		   (cmd "git clone %s %s" repo wsd)
		   ;; (if (file-exists-p "makefile")
		   ;; 	   (progn
		   ;; 		 (message "running make -k all in %s" wsd)
		   ;; 		 (cmd "make -k all")))
		   wsd)
		  (t
		   (message "You need to be online to clone the repository.") nil))))


;;; usage: (targ-gz-install ~/tmp/org-distros
;;; http://orgmode.org/org-8.2.10.tar.gz) Assumes url ends in tar.gz
;;; and untars into a directory org-8.2.10 returns directory
;;; containing distro if exists or successfully installed, nil if not
;;; online and wsd doesn't exists.

(defun tar-gz-install (base-dir url)
  "Installs and untars the tar-gz file at url at BASE-DIR.
  Returns that directory.  Returns nil if the operation fails."
        ;; directory of working copy
  (let* ((fname-tar-gz
		  (string-chop-prefix (url-filename (url-generic-parse-url url)) "/"))
		 (fname (string-chop-suffix fname-tar-gz ".tar.gz"))
		 (wsd (concat-as-dir base-dir fname)))
	(message "base-dir = %s" base-dir)
	(message "fname-tar-gz = %s" fname-tar-gz)
	(message "fname = %s" fname)
	(message "wsd = %s" wsd) 
	(cond ((file-exists-p wsd) (message "%s already installed. " wsd) wsd)
		  ((online-p)
		   (message "online.  wgetting ...")
		   (cmd "cd %s; wget %s; tar -xzvf %s; /bin/rm -rf %s"
				base-dir url fname-tar-gz fname-tar-gz)
		   wsd)
		  (t (message "You need to be online to install from url.") nil))))

;;; dir is an absolute path
;;; alist holds resources
(defun generic-install (dir alist)
  (dolist (p alist)
	(cond
	 ((git-bare-repo-p (cdr p))
	  (git-install dir (cdr p)  
				   (symbol-name (car p))))
	 ((tar-gz-repo-p (cdr p))
	  (tar-gz-install dir (cdr p)))
	 ((listp (cdr p))
	  (let ((dir (concat-as-dir dir (symbol-name (car p)))))
		(cmd "mkdir -p %s" dir)
		(generic-install dir (cdr p))))
	 (t (error "generic-install: invalid resource name %s" p)))))
		

;;; directory utils
;;; ===============

(defun symlink (dir target &optional name)
  (if name
	  (cmd "cd %s; ln -sfT %s %s" dir target name)
	(cmd "cd %s; ln -sf %s" dir target)))
  

;;; utility for parsing command line args of the form
;;; name=val ...
;;; into an association list (("name" . "val") ...)

;;; see
;;; https://emacs.stackexchange.com/questions/20998/why-is-this-trim-space-function-so-complicated-ugly-in-emacs-lisp

(defun trim-space (str)
    "Trim leading and trailing whitespace from STR."
    (replace-regexp-in-string
	 "\\(\\`[[:space:]\n]*\\|[[:space:]\n]*\\'\\)" "" str))


(defun parse-args (arg-string)
  (let ((arg-string (trim-space arg-string)))
	(if (string= "" arg-string)
		'()
		(let* ((args= (split-string arg-string " "))
			   (args-list (mapcar
						   (lambda (p) (split-string p "="))
						   args=))
			   (args-alist (mapcar (lambda (p) (apply 'cons p))
								   args-list)))
		  args-alist))))

(defun path-str-to-path-list (path-str)
   (split-string path-str ":"))



;;; get param from
;;; - command line, 
;;; - else value of symbol
;;; - else default
(defun get-param-bound-or-default (sym i &optional default)
  (cond ((> (length argv) i)
		 (let ((val (elt argv i)))
		   (message "%s (from command line) = %s" sym val)
		   val))
		
		((boundp sym)
		 (let ((val (symbol-value sym)))
		   (message "%s (from prj-config.el) = %s" sym val)
		   val))
		(t
		 (let ((val default))
		   (message "%s (default) = %s" sym val)
		   val))))


;;; get value from
;;; - command line parsed as an args-alist
;;; - else value of symbol
;;; - else default
(defun get-alist-bound-or-default (sym args-alist default)
  (let ((v (assoc sym args-alist)))
	(cond
	 (v (let ((val (cdr v)))
		  (message "%s (from command line) = %s" sym val)
		  val))
		
	 ((boundp sym)
	  (let ((val (symbol-value sym)))
		(message "%s (from prj-config.el) = %s" sym val)
		val))
	 (t
	  (let ((val default))
		(message "%s (default) = %s" sym val)
		val)))))


;;; parsing args string of the form n=v ...
;;; ========================================
(defun trim-space (str)
    "Trim leading and trailing whitespace from STR."
    (replace-regexp-in-string
	 "\\(\\`[[:space:]\n]*\\|[[:space:]\n]*\\'\\)" "" str))

;;; s is a string "arg1=val1 ..."
;;; returns ("arg1=val1" ...)
(defun n=v-str-to-n=v-list (s)
  (let* ((s (trim-space s)))
	(if (string= "" s)
		'()
	  (split-string s " "))))

;;; n=v-list: ("name=val" ...)
;;; returns ((name . val) ...)
(defun n=v-list-to-alist (n=v-list)
  (mapcar (lambda (p)
		  (let* ((ls (split-string p "="))
				 (name (intern (car ls)))
				 (val (cadr ls)))
			(cons name val)))
		n=v-list))


;;; s: "arg1=val1 ..."
;;; returns ((arg1  . val1) ...)
(defun n=v-str-to-alist (s)
  (let ((ls (n=v-str-to-n=v-list s)))
	(n=v-list-to-alist ls)))

(defun parse-n=v-list (ls)
  (n=v-list-to-alist ls))

(defun parse-n=v-str (s)
  (n=v-str-to-alist s))


;;; path:  "d1:d2 ..."
;;; returns: '("d1" "d2" ...)'

(defun path-to-dirs (p)
  (if (string= p "")
	  nil
	(split-string p ":")))

(defun lookup (k d &optional v)
  (let ((p (assoc k d)))
	(if p (cdr p)
	  v)))

;;; buffer functions
;;; append-file-to-buffer
(defun append-file-to-buffer (file b)
  "Append contents of FILE to buffer"
  (save-excursion
	(let ((buf (find-file (expand-file-name file))))
	  (switch-to-buffer b)
	  (end-of-buffer)
	  (insert-buffer-substring buf))))


(provide 'emacs-funs)



