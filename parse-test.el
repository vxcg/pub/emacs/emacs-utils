(message "entering test.el ...")
(message "argv = %s" argv)


;;; argv = "<elisp-dir>  <name>=<val> ..."

(let ((elisp-dir (car argv)))
  (setq load-path (cons elisp-dir load-path)))

(require 'parse-args)
(setq args-alist (parse-n=v-list (cdr argv)))
(message "args-alist = ")
(print args-alist)

(message "... exiting test.el")
